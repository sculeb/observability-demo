package de.cluetec.observability.service2;

import lombok.Data;

@Data
public class DepartmentSalaryInfo {

	private String department;
	private Integer avgSalary;
}
