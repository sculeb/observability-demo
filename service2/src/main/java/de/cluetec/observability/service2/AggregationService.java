package de.cluetec.observability.service2;

import java.util.ArrayList;
import java.util.List;

import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AggregationService {

	private final MongoTemplate mongoTemplate;

	private static final String FIELD_DEPARTMENT = "department";
	private static final String FIELD_SALARY = "salary";
	private static final String FIELD_AVG_SALARY = "avgSalary";

	@NewSpan("mongo-aggregation: getAvgSalaryInfo")
	public List<DepartmentSalaryInfo> getAvgSalaryInfo(String matchField, Object matchValue) {
		List<AggregationOperation> ops = new ArrayList<>();

		ops.add(Aggregation.match(Criteria.where(matchField).is(matchValue)));

		ops.add(Aggregation.group(FIELD_DEPARTMENT).avg(FIELD_SALARY).as(FIELD_AVG_SALARY).first(FIELD_DEPARTMENT)
				.as(FIELD_DEPARTMENT));
		Aggregation agg = Aggregation.newAggregation(ops);

		AggregationResults<DepartmentSalaryInfo> res = mongoTemplate.aggregate(agg, "employee",
				DepartmentSalaryInfo.class);

		return res.getMappedResults();
	}

}
