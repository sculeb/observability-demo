package de.cluetec.observability.service2;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class AnalysisController {

	private final AggregationService aggregationService;

	@GetMapping("/departmentAvgSalaryForAge")
	public List<DepartmentSalaryInfo> getAvgSalarayPerDepartmentForAge(@RequestParam Integer age) {
		return aggregationService.getAvgSalaryInfo("age", age);
	}

	@GetMapping("/departmentAvgSalaryForState")
	public List<DepartmentSalaryInfo> getAvgSalarayPerAgeForState(@RequestParam String state) {
		return aggregationService.getAvgSalaryInfo("state", state);
	}
}
