#!/bin/bash

cd service1
mvn -B -DskipTests=true clean package
cp target/service1.jar docker/service1.jar

cd ../service2
mvn -B -DskipTests=true clean package
cp target/service2.jar docker/service2.jar

cd ../service3
mvn -B -DskipTests=true clean package
cp target/service3.jar docker/service3.jar
