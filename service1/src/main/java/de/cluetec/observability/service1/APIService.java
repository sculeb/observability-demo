package de.cluetec.observability.service1;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class APIService {

	@Value("${services.service2}")
	private String service2;

	@Value("${services.service3}")
	private String service3;

	private final RestTemplate restTemplate;

	public List<DepartmentSalaryInfo> callService2(String path, String queryParam, Object queryValue) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(service2 + path) //
				.queryParam(queryParam, queryValue);
		return restTemplate.exchange(builder.build().toUri(), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DepartmentSalaryInfo>>() {
				}).getBody();
	}

	public void callService3() {
		restTemplate.getForObject(service3, String.class);
	}
}
