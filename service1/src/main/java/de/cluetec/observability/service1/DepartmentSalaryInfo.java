package de.cluetec.observability.service1;

import lombok.Data;

@Data
public class DepartmentSalaryInfo {

	private String department;
	private Integer avgSalary;
}
