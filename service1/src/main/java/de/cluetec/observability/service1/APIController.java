package de.cluetec.observability.service1;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class APIController {

	private final APIService apiService;

	@GetMapping("/endpoint1")
	public List<DepartmentSalaryInfo> getAvgSalarayPerDepartmentForAge(@RequestParam Integer age) {
		List<DepartmentSalaryInfo> depSalInfo = apiService.callService2("departmentAvgSalaryForAge", "age", age);
		apiService.callService3();
		return depSalInfo;
	}

	@GetMapping("/endpoint2")
	public List<DepartmentSalaryInfo> getAvgSalarayPerDepartmentForAge(@RequestParam String state) {
		List<DepartmentSalaryInfo> depSalInfo = apiService.callService2("departmentAvgSalaryForState", "state", state);
		apiService.callService3();
		return depSalInfo;
	}

}
