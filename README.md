# Observability Demo

## Overview

- 3 Spring Boot Services with Micrometer, Spring Cloud Sleuth & Zipkin
- mongodb
- InfluxDB
- Grafana
- Jaeger

## How to

- extract `data.tar.gz`

```shell
./build.sh
docker-compose up -d
 ```

- Fire requests at `http://localhost:8080/endpoint1?age=30` and `http://localhost:8080/endpoint2?state=Florida`
- Create dashboard in Grafana (`http://localhost:3000`)
- See tracing info in Jaeger (`http://localhost:16686`)
